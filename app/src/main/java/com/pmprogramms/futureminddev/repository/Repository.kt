package com.pmprogramms.futureminddev.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.pmprogramms.futureminddev.model.FutureMind
import com.pmprogramms.futureminddev.model.FutureMindEntity
import com.pmprogramms.futureminddev.retrofit.API
import com.pmprogramms.futureminddev.room.FutureMindDAO
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Repository(
    private val futureMindDAO: FutureMindDAO
) {
    private val url = "https://recruitment-task.futuremind.dev"

    private val service = Retrofit.Builder()
        .baseUrl(url)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(API::class.java)

    private lateinit var futureMindMutableLiveData: MutableLiveData<List<FutureMind>>

    var futureMindMutableLiveFromDatabase: LiveData<List<FutureMindEntity>> =
        futureMindDAO.getData()


    fun getDataOnline(): LiveData<List<FutureMind>> {
        futureMindMutableLiveData = MutableLiveData<List<FutureMind>>()
        
        service.getData().enqueue(object : Callback<List<FutureMind>> {
            override fun onResponse(
                call: Call<List<FutureMind>>,
                response: Response<List<FutureMind>>
            ) {
                if (response.isSuccessful) {
                    futureMindMutableLiveData.postValue(response.body())
                } else
                    futureMindMutableLiveData.postValue(null)
            }

            override fun onFailure(call: Call<List<FutureMind>>, t: Throwable) {
                futureMindMutableLiveData.postValue(null)
            }
        })

        return futureMindMutableLiveData
    }

    suspend fun insertData(futureMindEntity: FutureMindEntity) {
        futureMindDAO.insertData(futureMindEntity)
    }
}