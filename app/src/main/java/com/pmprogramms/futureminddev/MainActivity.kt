package com.pmprogramms.futureminddev

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.pmprogramms.futureminddev.databinding.ActivityMainBinding
import com.pmprogramms.futureminddev.model.FutureMindEntity
import com.pmprogramms.futureminddev.util.Network
import com.pmprogramms.futureminddev.util.RecyclerViewAdapter
import com.pmprogramms.futureminddev.viewmodel.ViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: ViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this).get(ViewModel::class.java)

        Toast.makeText(this, "Loading data...", Toast.LENGTH_SHORT).show()

        loadData()

        binding.refreshButton.setOnClickListener {
            Toast.makeText(this, "Refreshing...", Toast.LENGTH_SHORT).show()
            loadData()
        }
    }

    private fun loadData() {
        if (Network().connected(this)) {
            getDataOnline()
        } else {
            readFromDB()
        }
    }

    private fun getDataOnline() {
        viewModel.getDataOnline().observe(this, { data ->
            if (data != null) {
                for (i in data.indices) {
                    val modelEntity = FutureMindEntity(
                        0,
                        data[i].title,
                        data[i].description,
                        data[i].image_url,
                        data[i].modificationDate,
                        data[i].orderId
                    )
                    viewModel.insertData(modelEntity)
                }
            }
            readFromDB()
        })
    }

    private fun readFromDB() {
        viewModel.readDataFromDatabase.observe(this, { data ->
            if (data != null) {
                binding.recyclerView.apply {
                    adapter = RecyclerViewAdapter(data)
                    layoutManager = LinearLayoutManager(this@MainActivity)
                    setHasFixedSize(true)
                }
            }
        })
    }
}
