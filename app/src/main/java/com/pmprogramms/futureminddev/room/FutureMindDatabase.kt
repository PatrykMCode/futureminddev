package com.pmprogramms.futureminddev.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.pmprogramms.futureminddev.model.FutureMindEntity

@Database(entities = [FutureMindEntity::class], version=1,exportSchema=false)
abstract class FutureMindDatabase : RoomDatabase() {
    abstract fun databaseDao(): FutureMindDAO

    companion object {
        private var INSTANCE: FutureMindDatabase? = null

        fun getDatabase(context: Context): FutureMindDatabase {
            val tmpInstance = INSTANCE

            if (tmpInstance != null)
                return tmpInstance

            synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext,
                    FutureMindDatabase::class.java,
                    "future_mind_database.db")
                    .build()
                INSTANCE = instance


                return instance
            }
        }
    }
}