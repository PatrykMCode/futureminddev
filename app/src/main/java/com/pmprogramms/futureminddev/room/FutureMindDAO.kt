package com.pmprogramms.futureminddev.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pmprogramms.futureminddev.model.FutureMindEntity

@Dao
interface FutureMindDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertData(model: FutureMindEntity)

    @Query("SELECT * FROM future_mind_table ORDER BY orderId")
    fun getData() : LiveData<List<FutureMindEntity>>
}