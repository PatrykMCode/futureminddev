package com.pmprogramms.futureminddev.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.pmprogramms.futureminddev.model.FutureMind
import com.pmprogramms.futureminddev.model.FutureMindEntity
import com.pmprogramms.futureminddev.repository.Repository
import com.pmprogramms.futureminddev.room.FutureMindDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModel(application: Application): AndroidViewModel(application) {
    val readDataFromDatabase: LiveData<List<FutureMindEntity>>

    private val repository: Repository

    init {
        val futureMindDAO = FutureMindDatabase.getDatabase(application).databaseDao()
        repository = Repository(futureMindDAO)

        readDataFromDatabase = repository.futureMindMutableLiveFromDatabase
    }

    fun getDataOnline() : LiveData<List<FutureMind>> {
        return repository.getDataOnline()
    }

    fun insertData(futureMindEntity: FutureMindEntity) {
        viewModelScope.launch(Dispatchers.IO){
            repository.insertData(futureMindEntity)
        }
    }
}