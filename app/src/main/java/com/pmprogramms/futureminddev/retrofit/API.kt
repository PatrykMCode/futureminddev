package com.pmprogramms.futureminddev.retrofit

import com.pmprogramms.futureminddev.model.FutureMind
import retrofit2.Call
import retrofit2.http.GET

interface API {
    @GET("/recruitment-task")
    fun getData() : Call<List<FutureMind>>
}