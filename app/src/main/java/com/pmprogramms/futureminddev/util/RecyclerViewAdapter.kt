package com.pmprogramms.futureminddev.util

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.pmprogramms.futureminddev.WebActivity
import com.pmprogramms.futureminddev.databinding.RecyclerItemBinding
import com.pmprogramms.futureminddev.helper.TextHelper
import com.pmprogramms.futureminddev.model.FutureMindEntity

class RecyclerViewAdapter(
    private val arrayData: List<FutureMindEntity>
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            RecyclerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(arrayData[position])
    }

    override fun getItemCount(): Int {
        return arrayData.size
    }

    class ViewHolder(private val binding: RecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(futureMindEntity: FutureMindEntity) {
            val description = TextHelper.splitURLFromDescription(futureMindEntity.description)[0]
            val url = TextHelper.splitURLFromDescription(futureMindEntity.description)[1]

            binding.titleText.text = futureMindEntity.title
            binding.descriptionText.text = description
            binding.dateText.text = TextHelper.formatDate(futureMindEntity.modificationDate)

            Glide.with(binding.root).load(futureMindEntity.image_url)
                .apply(RequestOptions().override(100, 100)).into(binding.image)

            binding.container.setOnClickListener {
                val intent = Intent(binding.root.context, WebActivity::class.java)
                intent.putExtra("url", url)
                binding.root.context.startActivity(intent)
            }
        }
    }
}