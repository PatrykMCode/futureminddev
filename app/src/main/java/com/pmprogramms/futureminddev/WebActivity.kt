package com.pmprogramms.futureminddev

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.pmprogramms.futureminddev.databinding.ActivityWebBinding

class WebActivity : AppCompatActivity() {
    private lateinit var binding: ActivityWebBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWebBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val url = intent.getStringExtra("url")

        if (url != null) {
            binding.webView.loadUrl(url)
            binding.webView.settings.builtInZoomControls = true
        } else {
            Toast.makeText(this, "Can't open empty url", Toast.LENGTH_SHORT).show()
        }
    }
}