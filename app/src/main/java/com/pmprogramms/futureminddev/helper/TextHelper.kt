package com.pmprogramms.futureminddev.helper

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class TextHelper {
    companion object {
        fun splitURLFromDescription(description: String): List<String> {
            return description.split("\t")
        }

        fun formatDate(dateString: String): String {
            val formatter: DateFormat = SimpleDateFormat("yyyy-MM-DD", Locale.UK)
            val date = formatter.parse(dateString) as Date
            val newFormat = SimpleDateFormat("dd.MM.yyyy", Locale.UK)
            return newFormat.format(date)
        }
    }
}