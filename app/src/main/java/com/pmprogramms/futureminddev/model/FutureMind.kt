package com.pmprogramms.futureminddev.model

data class FutureMind(
    val description: String,
    val image_url: String,
    val modificationDate: String,
    val orderId: Int,
    val title: String
)