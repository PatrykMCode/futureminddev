package com.pmprogramms.futureminddev.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "future_mind_table")
data class FutureMindEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val title: String,
    val description: String,
    val image_url: String,
    val modificationDate: String,
    val orderId: Int
)